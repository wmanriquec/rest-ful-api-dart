import 'package:aqueduct/aqueduct.dart';

class User {
  int id;
  String name;
  String password;
  String email;

  User({
    this.id,
    this.name,
    this.password,
    this.email,
  });

  //@override
  Map<String, dynamic> asMap() {
    return {
      "id"    : id,
      "name"  : name,
      "email" : email
    };
  }
  factory User.fromMap(Map<String, dynamic> json) => new User(
    id: json["id"] as int,
    name: json['name'] as String,
    password: json['password'] as String,
    email: json['email'] as String,
  );
  // ignore: avoid_renaming_method_parameters
}
