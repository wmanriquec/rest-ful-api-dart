import 'package:mysql1/mysql1.dart';

import 'controllers/ProductController.dart';
import 'controllers/UserController.dart';
import 'restapi.dart';

class RestapiChannel extends ApplicationChannel {
  /// Initialize services in this method.
  ///
  /// Implement this method to initialize services, read values from [options]
  /// and any other initialization required before constructing [entryPoint].
  ///
  /// This method is invoked prior to [entryPoint] being accessed.
  @override
  Future prepare() async {
    logger.onRecord.listen((rec) => print("$rec ${rec.error ?? ""} ${rec.stackTrace ?? ""}"));
  }

  /// Construct the request channel.
  ///
  /// Return an instance of some [Controller] that will be the initial receiver
  /// of all [Request]s.
  ///
  /// This method is invoked after [prepare].
  @override
  Controller get entryPoint {
    final router = Router();

    // Prefer to use `link` instead of `linkFunction`.
    // See: https://aqueduct.io/docs/http/request_controller/
    router.route("/example").linkFunction((request) async {
      return Response.ok({"key": "value"});
    });
    
    router.route("/user[/:id]").link(()=>UserController());
    router.route("/products[/:id]").link(()=>ProductController());

    router.route("/user/login").linkFunction((request) async {
      if (request.method == 'POST'){
        Map<String, dynamic> user = await request.body.decode();
        final conn = await MySqlConnection.connect(ConnectionSettings(
          host: 'localhost', port: 3306, user: 'root', password: '974777331', db: 'tmda'));
        Results results = await conn
          .query('select * from users where email = ? and password = ?', [user["email"],user["password"]]);
        for (var row in results) {
          return Response.ok({'state': 'nice', 'values':{'id': row[0], 'name': row[1], 'email': row[2]}});
        }
        return Response.ok({'state': 'error'});
      }
      return Response(405, null, 'Erroneo');
    });

    return router;
  }

}