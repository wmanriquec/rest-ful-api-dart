
import 'dart:convert';

import '../restapi.dart';

import 'package:mysql1/mysql1.dart';

class ProductController extends ResourceController   {

  @Operation.get() 
  Future<Response> getAllProducts() async {
    final conn = await MySqlConnection.connect(ConnectionSettings(
      host: 'localhost', port: 3306, user: 'root', password: '974777331', db: 'tmda'));
    Results results = await conn
      .query('select * from products');
    Map temp={'state':'nice','values':[]};
    for (var row in results) {
      temp['values'].add({'id': row[0], 'name': row[1], 'price': row[2], 'description': row[3]});
    }

    //print(response);
    return Response.ok(temp);
    
  }
  

  @Operation.post('id') 
  Future<Response> getProduct(@Bind.path("id") int idx) async => Response.ok('viendo Product $idx');
  
 
  @Operation.post() 
  Future<Response> addProduct(@Bind.body() Map product) async {
    final conn = await MySqlConnection.connect(ConnectionSettings(
      host: 'localhost', port: 3306, user: 'root', password: '974777331', db: 'tmda'));
    var result = await conn.query(
        'insert into products (name, description, price) values (?, ?, ?)',
        [product['name'], product['description'], product['price']]);
    if (result.insertId != null){
      return Response.ok({'state': 'nice'});
    }
    return Response.ok({'state': 'error'});
  }
  @Operation.put("id") 
  Future<Response> updateProduct(@Bind.body() Map product, @Bind.path("id") int idx) async {
    final conn = await MySqlConnection.connect(ConnectionSettings(
      host: 'localhost', port: 3306, user: 'root', password: '974777331', db: 'tmda'));
    var result = await conn.query(
        'update products set name = ? , description = ?, price = ? where id = ?',
        [product['name'], product['description'], product['price'], idx]);
    if (result.affectedRows == 1){
      return Response.ok({'state': 'nice'});
    }
    return Response.ok({'state': 'error'});
  }
  @Operation.delete("id") 
  Future<Response> deleteProduct(@Bind.path("id") int idx) async {
    final conn = await MySqlConnection.connect(ConnectionSettings(
      host: 'localhost', port: 3306, user: 'root', password: '974777331', db: 'tmda'));
    var result = await conn.query(
        'DELETE FROM products where id = ?',
        [idx]);
    if (result.affectedRows == 1){
      return Response.ok({'state': 'nice'});
    }
    return Response.ok({'state': 'error'});
  }
  
}