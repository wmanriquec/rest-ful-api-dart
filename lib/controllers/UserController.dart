
import 'dart:convert';

import 'package:mysql1/mysql1.dart';
import 'package:restapi/models/user.dart';

import '../restapi.dart';

class UserController extends ResourceController   {

  @Operation.get() 
  Future<Response> getAllUsers() async { 
    final conn = await MySqlConnection.connect(ConnectionSettings(
      host: 'localhost', port: 3306, user: 'root', password: '974777331', db: 'tmda'));
    Results results = await conn
      .query('select * from users');
    String s =results.toString();
    s=s.replaceAll('Fields: ','');
    s=s.replaceAll('(','');
    s=s.replaceAll(')','');
    print(s);
    return Response.ok(s);
  }
  
  
  @Operation.get('id')
  Future<Response> getUsers(@Bind.path("id") int idx) async {
    final conn = await MySqlConnection.connect(ConnectionSettings(
      host: 'localhost', port: 3306, user: 'root', password: '974777331', db: 'tmda'));
    Results results = await conn
      .query('select * from users');
    String s =results.toString();
    s=s.replaceAll('Fields: ','');
    s=s.replaceAll('(','');
    s=s.replaceAll(')','');
    print(s);
    return Response.ok(s);
  }
  
 
  @Operation.post() 
  Future<Response> insert(@Bind.body() Map user) async {
    
    final conn = await MySqlConnection.connect(ConnectionSettings(
      host: 'localhost', port: 3306, user: 'root', password: '974777331', db: 'tmda'));
    var result = await conn.query(
        'insert into users (name, email, password) values (?, ?, ?)',
        [user['name'], user['email'], user['password']]);
    if (result.insertId != null){
      return Response.ok({'state': 'nice'});
    }
    return Response.ok({'state': 'error'});
  }
  
}