/// restapi
///
/// A Aqueduct web server.
library restapi;

export 'dart:async';
export 'dart:io';

export 'package:aqueduct/aqueduct.dart';

export 'channel.dart';
